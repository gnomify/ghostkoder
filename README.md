# ghostkoder

![screenshot](https://gitlab.com/gnomify/ghostkoder/-/raw/images/ghostkoder.png)

ghostkoder - Survive GTK4 Python3 dungeon with chatgpt

## Introduction

chatgpt has a convenient code generation feature, but it takes dozens of copy and paste attempts before the GTK4/python code works. This tool automatically repeats the process until the error is resolved to improve efficiency.

## Features

### Manual Mode (Basic)

1. Developer sends prompt
1. chatgpt generates code
1. Developer manually copy code written by chatgpt
1. ghostkoder detects clipboard automatically and run it as python code
1. ghostkoder Sends errors / warnings to clipboard automatically
1. Developer manually pastes errors/warnings into message input form on chatgpt web page
1. chatgpt re-writes code
1. Loop from 3rd

### Auto Mode (Advanced)

TBW (To.Be.Written)

### Giadow Reload (Black Magic)

TBW

### Integrate ghostkoder into your own app

TBW

### Note

- Do not do any other work with ghostkoder running on your PC as it uses the clipboard.
- Both python and webdriver crash frequently because they are doing new things.
- Contribution is not accepted, so fork it on your own.