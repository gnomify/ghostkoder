import gi
gi.require_version('Gtk', '4.0')
from gi.repository import Gtk, Gio

def main():
    app = Gtk.Application.new("org.example.myapp", Gio.ApplicationFlags.FLAGS_NONE)
    app.connect("activate", on_activate)
    
    app.run([])

def on_activate(app):
    window = Gtk.Window(application=app)
    window.set_title("Hello ghostkoder")
    window.set_default_size(200, 100)

    window.show()

if __name__ == "__main__":
    main()
